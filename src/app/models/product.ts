export class Product {
    $key: string;
    name: string;
    description:string;
    imagen: string;
    price: number;
}
