import { Injectable } from '@angular/core';
import {AngularFireDatabase, AngularFireList} from 'angularfire2/database';
import { Product } from '../models/product';

@Injectable({
  providedIn: 'root'
})
export class ProductsService {
  private productLista:AngularFireList<any>;
  constructor(private firebase:AngularFireDatabase) {
    this.productLista=this.firebase.list('product');
   }
   getAllProducts(){
     return this.productLista=this.firebase.list('product');
   }
   saveProduct(product:Product){
      var result=this.productLista.push( {name:product.name,description:product.description,imagen:product.imagen,price:product.price});
      var displayDate = new Date().toLocaleDateString();

      console.log("save:"+result+ "  "+displayDate);
   }
}
