import { Injectable } from '@angular/core';
import {AngularFireList,AngularFireDatabase} from 'angularfire2/database';
@Injectable({
  providedIn: 'root'
})
export class OrdersService {
  orderList:AngularFireList<any>;
  constructor(private firebase:AngularFireDatabase) { 
    this.orderList=this.firebase.list('orders');
  }

  getAllOrders(){
    return this.orderList;
  }

  saveOrder(){
    /*
    var key= this.orderList.push({total:1231,date:new Date().toLocaleDateString()}).then((value)=>{
      console.log("value key"+value.key);
    });
    */
   return this.orderList.push({total:1231,date:new Date().toLocaleDateString()});
  }
  updateOrder(key:string,total:number){
    this.orderList.update(key,{total:total});
  }
  
}
