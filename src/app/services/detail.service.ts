import { Injectable } from '@angular/core';
import {AngularFireDatabase,AngularFireList} from 'angularfire2/database';
import { Detail } from '../models/detail';

@Injectable({
  providedIn: 'root'
})
export class DetailService {
  detailList:AngularFireList<any>;

  constructor(private firebase:AngularFireDatabase) { 
    this.detailList=this.firebase.list('detail');
  }

  getAllDetail(){
    return this.detailList;
  }

  setDetail(){
    this.detailList.push({idProduct:1,idOrder:2,cant:123});
  }

  setDetailForItem(detail:Detail){
    this.detailList.push({idProduct:detail.idProduct,idOrder:detail.idOrder,cant:detail.cant});
  }
}
