import { Component, OnInit, ViewChild } from '@angular/core';
import {ProductsService} from '../../services/products.service';
import {OrdersService} from '../../services/orders.service';
import {DetailService} from '../../services/detail.service';
import {Product} from '../../models/product';
import {Detail} from '../../models/detail';
import { NgForm } from '@angular/forms';
import { isUndefined, isString, isNull } from 'util';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit {
  productList:any=[];
  listSales:any=[];
  cont:number=0;
  product:Product=new Product();

  constructor(private productService:ProductsService,private ordersService:OrdersService,private detailService:DetailService) {
    this.productService.getAllProducts().snapshotChanges().subscribe(item => {
      this.productList = [];
      item.forEach(element => {
        let x = element.payload.toJSON() as Product;
        x.$key=element.key;
        this.productList.push(x);
      });
    });
   }

  ngOnInit() {
  }

  selectProduct(product:Product){
    var bandera=false;
    console.log(this.listSales);
    if(this.cont>0){
      this.listSales.forEach(element => {
        if(element.length>0){
          if(element[0].$key==product.$key){
            element[1]=1+element[1]as number;
            bandera=true;  
          }
        }
      });
    }
    if (bandera==false) {
      var ar:any=[];
      ar.push(product);
      ar.push(1);
      this.listSales.push(ar);
      this.cont++;
    }    
  }

  save(){
    //this.productService.saveProduct();
  }

  limpiar(){
    this.cont=0;
    this.listSales=[];
  }

  savePedido(){
    ///agregar el  order a la db
    var idOrder=this.ordersService.saveOrder().key;
    console.log("order: "+idOrder);
    ///mejora la lista detail
    var detailList:any=[];
    var total:number=0;
    this.listSales.forEach(element => {
      var detalle=new Detail();
      detalle.idProduct=element[0].$key;
      detalle.cant=element[1];
      detalle.idOrder=idOrder;
      total=total+element[0].price*element[1];
      detailList.push(detalle);
    });
    ///agregando el detalle al a db
    detailList.forEach(element => {
      this.detailService.setDetailForItem(element as Detail);
    });
    ///actualizando la order total en la db
    this.ordersService.updateOrder(idOrder,total);
    alert("Pedido Registrador !!!");
    this.limpiar();
  }
  onSubmit(productForm:NgForm){
    if(isUndefined(this.product.price)||isNull(this.product.price)){
      this.product.price=10;
    }
    if(isUndefined(this.product.name)){
      this.product.name="producto";
    }
    if(isUndefined(this.product.imagen)||this.product.imagen.toString().search(".jpg/.png/.gif")==-1){
      this.product.imagen="http://www.recetahamburguesa.com/ImagenesRecetaHamburguesa/ImagenesRecetaHamburguesa/receta-hamburguesa-thermomix.jpg";
    }
    if(isUndefined(this.product.description)){
      this.product.description="este es un producto de HotBuger";
    }
    this.productService.saveProduct(this.product);
    productForm.reset();
    ///cerrar el modal
    document.getElementById('closeModal').click();
  }
}
