import { Component, OnInit } from '@angular/core';
import { OrdersService } from '../../services/orders.service';
import { DetailService } from '../../services/detail.service';
import { ProductsService } from '../../services/products.service';
import {Orders} from '../../models/orders';
import { Detail } from '../../models/detail';
import { Product } from '../../models/product';

@Component({
  selector: 'app-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.scss']
})
export class OrdersComponent implements OnInit {
  ordersList:any=[];
  productList:any=[];
  detailList:any=[];
  productFilter:any=[];
  detalleOrder:any=[];

  constructor(private ordersService:OrdersService,private detailService:DetailService,private productService:ProductsService) { 
  }

  ngOnInit() {
    ///get all ordersServices
    this.ordersService.getAllOrders().snapshotChanges().subscribe(item => {
      this.ordersList = [];
      item.forEach(element => {
        let x = element.payload.toJSON() as Orders;
        x.$key=element.key.toString();
        this.ordersList.push(x);
      });
    });
    ///get all products
    this.productService.getAllProducts().snapshotChanges().subscribe(item => {
      this.productList = [];
      item.forEach(element => {
        let x = element.payload.toJSON() as Product;
        x.$key=element.key.toString();
        this.productList.push(x);
      });
    });
    ///get all detail
    this.detailService.getAllDetail().snapshotChanges().subscribe(item => {
      this.detailList = [];
      item.forEach(element => {
        let x = element.payload.toJSON() as Detail;
        x.$key=element.key.toString();
        this.detailList.push(x);
      });
    });
  }

  addOrder(){
    var orderkey=this.ordersService.saveOrder();
    alert("save "+orderkey.key);
  }

  addDetail(){
    this.detailService.setDetail();
    alert("save detail");
  }
  
  verDetalle(order:string){
    this.productFilter=[];  
    this.detalleOrder=[];
    ///this.  productFilter=this.detailService.getAllById(this.detalleOrder.key);
    this.detailList.forEach(element => {
      if(element.idOrder==order){
        this.detalleOrder.push(element as Detail);
      }
    });
    this.detalleOrder.forEach(elementDetai => {
      this.productList.forEach(elementProd => {
        if(elementDetai.idProduct==elementProd.$key){
          this.productFilter.push({name:elementProd.name,cant:elementDetai.cant});
          console.log(elementProd);
        }
      });
    });
    
  }
}
