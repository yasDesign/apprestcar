import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { ProductsComponent } from './components/products/products.component';
import { OrdersComponent } from './components/orders/orders.component';
import {FormsModule} from '@angular/forms'
//angular
import {environment} from '../environments/environment';
import{AngularFireModule} from 'angularfire2';
import {AngularFireDatabaseModule} from 'angularfire2/database';

//servicios
import {ProductsService} from './services/products.service';
import {OrdersService} from './services/orders.service';
import {DetailService} from './services/detail.service';


@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    ProductsComponent,
    OrdersComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    AngularFireModule.initializeApp(environment.configfirebase)
    ,AngularFireDatabaseModule,
    FormsModule
  ],
  providers: [ProductsService,DetailService,OrdersService],
  bootstrap: [AppComponent]
})
export class AppModule { }
